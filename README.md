![Pharo](http://pharo.org/web/files/pharo.png)


# WillowPagerDuty

[![build status](https://gitlab.com/macta/WillowPagerDuty/badges/master/build.svg)](https://gitlab.com/macta/WillowPagerDuty/) 


## Overview
This is a simple example of a [Willow](https://github.com/ba-st/Willow) Single Page Application that uses the  [PagerDuty](www.pagerduty.com) API to retrieve on-call schedule information and calculate hours on call.

As this is a smallish, realistic application, my hope is that it will help others better understand some of the more advanced concepts presented in the Willow playground.

You will need a valid PagerDuty account, and a generated API token from that account to login. This version of the application also hard-codes the ScheduleId's to query on call information - so you would need to edit these or improve the processing to dynamically detect what schedules to query for results.