"CmdLine script to clean a minimal image"

| logger repo version |

logger := FileStream stderr.
logger cr; nextPutAll: 'Starting Minimal Cleanup Script...'.

logger cr; nextPutAll: '>Resetting Class Comments'.
Smalltalk allClasses do: [ :c | c classComment: '' stamp: '' ].

logger cr; nextPutAll: '>Removing Clearing MC Registry'.
MetacelloProjectRegistration resetRegistry.

logger cr; nextPutAll: '>Removing Configurations'.
(Smalltalk at: #ConfigurationOf) allSubclasses do: [ :c |
    c name = #BaselineOf ifFalse: [ c removeFromSystem. logger nextPut: $+  ]].

"Careful, this will force #isLetter, #isUppercase #isLowercase, #toLowercase and #toUppercase to only work on ascii"
"
logger cr; nextPutAll: '>Removing Unicode definitions'.
Unicode classPool at: #GeneralCategory put: nil.
Unicode classPool at: #DecimalProperty put: nil.
"

logger cr; nextPutAll: '>ImageCleaner release routines'.
Smalltalk organization removeEmptyCategories.
logger nextPut: $..
Smalltalk
		allClassesAndTraitsDo: [ :class |
			[ :each |
				each
					removeEmptyCategories;
					sortCategories ]
						value: class organization;
						value: class class organization ].
logger nextPut: $..
(RPackageOrganizer default packages select: #isEmpty)
    do: #unregister.
logger nextPut: $..
Smalltalk organization sortCategories.
Smalltalk garbageCollect.
Smalltalk cleanOutUndeclared.
Smalltalk fixObsoleteReferences.
logger nextPut: $..
Smalltalk cleanUp: true except: { HashedCollection } confirming: false.

logger cr; nextPutAll: '>GC'.
3 timesRepeat: [
        Smalltalk garbageCollect.
        Smalltalk cleanOutUndeclared.
        Smalltalk fixObsoleteReferences].

logger cr; nextPutAll: 'Finished Script.'; cr; cr.