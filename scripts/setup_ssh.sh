#!/bin/sh
# Shell script to setup SSH - NOTE: must be called via 'source setup_ssh.sh'

set -veu

echo -e "\e[1m\nEstablishing SSH keys...\e[0m"
# install ssh-agent if needed
which ssh-agent || (apt-get install openssh-client -y --no-install-recommends)

# run ssh-agent
eval $(ssh-agent -s)

# add ssh key stored in AUTO_DEPLOY_PRIVATE_KEY variable to the agent store
ssh-add <(echo "$PRIVATE_KEY")

# disable host key checking (NOTE: makes you susceptible to man-in-the-middle attacks)
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

echo -e "\e[1m\nKeys installed.\e[0m\n"