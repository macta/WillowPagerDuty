"Configure the final environment"

| logger |
logger := FileStream stderr.
logger cr; nextPutAll: 'Configuring final environment...'.

"Stop logging changes and reading source files for read/only environment"
Smalltalk at: #NoChangesLog ifPresent: [  :c | c install ].
Smalltalk at: #NoPharoFilesOpener ifPresent: [  :c | c install ].

logger cr; nextPutAll: 'OK'; cr.