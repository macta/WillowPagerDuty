#!/bin/sh
# Shell script to deploy to Digital Ocean

set -veu

echo -e "\e[1m\nInitiating Transfer...\n\e[0m"

if [ ! -e deploy ]; then
    echo -e "\e[91m\nMissing Deployment Assets!\n\e[0m"; exit 1;
fi

if [ -z "$PROJECT_NAME" ]; then
    echo -e "\e[91m\nMissing VARIABLE definitions in Pipeline CI Settings!\n\e[0m";
    exit 1;
fi

cd deploy
ls -alh

echo -e "\e[1m\nTransferring assets to remote server...\e[0m"
scp $PROJECT_NAME.zip $DOMAIN_LOGIN:~

echo -e "\e[1m\nExtracting assets and restarting remote...\n\e[0m"
ssh -T -o StrictHostKeyChecking=no $DOMAIN_LOGIN << EOSSH
set -e

unzip -o $PROJECT_NAME.zip -d $PROJECT_NAME

cd $PROJECT_NAME
find . -name PharoDebug.log -maxdepth 1 -exec mv {} {}.bak \;
touch PharoDebug.log
echo -e "\nApplication directory contents:\n"
ls -al

cp $PROJECT_NAME.ini /etc/supervisor/
cp $PROJECT_NAME.nginx /etc/nginx/locations/

echo -e "\n"
supervisorctl update
supervisorctl restart $PROJECT_NAME

sudo service nginx reload

EOSSH

if (! ssh $DOMAIN_LOGIN "supervisorctl status $PROJECT_NAME" | grep -F "RUNNING"); then
    echo -e "\e[91m\nERROR restarting $PROJECT_NAME!\n\e[0m";
    ssh $DOMAIN_LOGIN "cd $PROJECT_NAME; pharo $PROJECT_NAME.image --no-default-preferences run.st";
    echo -e "\e[91m\nERROR $PROJECT_NAME not running!\n\e[0m";
    exit 1;
fi

echo -e "\e[32m\nDeploy Complete!\n\e[0m"

