"
I provide a simple login mechanism to tha app that require a PagerDuty api token to identify yourself.
"
Class {
	#name : #WPPagerDutyLoginView,
	#superclass : #WPWebView,
	#instVars : [
		'tokenField',
		'form',
		'submitButton'
	],
	#category : #PagerDuty
}

{ #category : #initialization }
WPPagerDutyLoginView >> initialize [

	super initialize.
	
	self
		initializeTokenField;
		initializeForm
]

{ #category : #initialization }
WPPagerDutyLoginView >> initializeForm [
	| gridView |
	
	submitButton := self componentSupplier
		asynchronicButtonLabeled: 'Login'
		applying: [ :anchor | anchor addClass bootstrap buttonPrimary ].

	gridView := self componentSupplier gridBuilder
		forSmallDevices;
		columnsPerRow: 1;
		"applyingToCells: (ComponentClassificationCommand toStyleWith: BootstrapCssStyles formGroup   );"
		addContent: (self labeledName: 'Token:' field: tokenField);
		addContent: submitButton;
		build.

	form := IdentifiedWebView
		forFormNamed: 'edit-token'
		containing: gridView.

	submitButton onTrigger
		serializeForm: form;
		evaluate: [ self session
				loginWith: tokenField contents ];
		render: self session identifiedContentView
]

{ #category : #initialization }
WPPagerDutyLoginView >> initializeTokenField [
	tokenField := self componentSupplier
		singleLineTextFieldApplying:
		 [ :field | (field setMaximumLengthTo: 30) ].
		
	
]

{ #category : #initialization }
WPPagerDutyLoginView >> labeledName: aString field: aField [
	^ LabeledWebView
		displaying: aString
		boundTo: aField
		applying: [ :field | field addClass bootstrap controlLabel ]
]

{ #category : #configuring }
WPPagerDutyLoginView >> onTrigger [

	^ submitButton onTrigger 
]

{ #category : #rendering }
WPPagerDutyLoginView >> renderContentOn: aCanvas [
	| mainView |
	mainView := IdentifiedWebView
		forDivNamed: 'login'
		containing: [ :canvas | 
			aCanvas
				paragraph: [ :html | 
					html text: 'To Login, enter your project '.
					html anchor
						url: 'https://app.pagerduty.com/';
						with: 'PagerDuty API token' ].
			aCanvas render: form ]
		applying: [ :div |  ].
	aCanvas render: mainView
]
