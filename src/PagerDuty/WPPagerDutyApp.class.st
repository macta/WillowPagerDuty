"
I am the main starting point for a Willow Web Application.

I define  the  main Session class to use and am used to register the application with Seaside.
"
Class {
	#name : #WPPagerDutyApp,
	#superclass : #WillowApplication,
	#instVars : [
		'contentView'
	],
	#category : #PagerDuty
}

{ #category : #accessing }
WPPagerDutyApp class >> applicationTitle [

	^ 'Pager Duty'
]

{ #category : #accessing }
WPPagerDutyApp class >> canBeRoot [
	^ true
]

{ #category : #accessing }
WPPagerDutyApp class >> handlerName [

	^ 'pdw'
]

{ #category : #private }
WPPagerDutyApp class >> installFileHandlerAccordingTo: theDeploymentMode [

	theDeploymentMode installApplicationFileHandlerFor: self
	
]

{ #category : #private }
WPPagerDutyApp class >> registerAsApplicationUsing: deploymentModeClass [
	| application |
	
	application := super registerAsApplicationUsing: deploymentModeClass.
	
	"increase the session timeout, so Ajax calls don't fail so quickly"
	application configuration at: #maximumRelativeAge put: 3600.
	
	^application
]

{ #category : #accessing }
WPPagerDutyApp class >> sessionClass [

	^ WPPagerDutySession 
]

{ #category : #accessing }
WPPagerDutyApp class >> start [

	self registerAsDevelopmentApplication.
	self startOnPort: 8080
]

{ #category : #accessing }
WPPagerDutyApp class >> startForProduction: port [
	| application |

	self stop.

	application := self registerAsDeployedApplication.

	"application preferenceAt: #serverPath put: '/app/' , self handlerName."
		
		"preferenceAt: #serverProtocol put: 'http';
    	preferenceAt: #serverHostname put: 'localhost';
    	preferenceAt: #serverPort put: 8080;"
	
	self startOnPort: port
]

{ #category : #accessing }
WPPagerDutyApp class >> startOnPort: aNumber [
	
	(ZnZincServerAdaptor port: aNumber)
		codec: GRPharoUtf8Codec new;
		start
]

{ #category : #accessing }
WPPagerDutyApp class >> stop [
	ZnServer stopDefault.
	ZnZincServerAdaptor allInstancesDo: [ :each | each shutDown ].
	
	self unregisterFromApplications.
	
	Smalltalk garbageCollect
]

{ #category : #accessing }
WPPagerDutyApp >> componentSupplierForApplication [

	^ BootstrapComponentSupplier
		withBootstrapLibrary: (self deploymentMode libraryFor: Bootstrap3MetadataLibrary) withoutOptionalTheme
		selectLibrary: ((self deploymentMode libraryFor: BootstrapSelectLibrary) using: self language)
		datepickerLibrary: ((self deploymentMode libraryFor: BootstrapDatepickerLibrary) using: self language)
		typeaheadLibrary: (self deploymentMode libraryFor: BootstrapTypeaheadLibrary) default
]

{ #category : #accessing }
WPPagerDutyApp >> contentView [

	^ contentView
]

{ #category : #hooks }
WPPagerDutyApp >> initialRequest: request [
	super initialRequest: request.

	self initializeContentView
]

{ #category : #initialization }
WPPagerDutyApp >> initializeContentView [
	
	contentView := IdentifiedWebView
		forDivNamed: 'application'
		containing: [ :canvas | 
			canvas
				render: WPPagerDutyMenuView new;
				render: self session identifiedContentView ]
		applying: [ :div :constants | div addClass bootstrap container ]
]

{ #category : #accessing }
WPPagerDutyApp >> jQueryLibrary [

	^ (self deploymentMode libraryFor: JQuery3MetadataLibrary) default
]

{ #category : #updating }
WPPagerDutyApp >> requiredLibraries [

	^{SpinKitMetadataLibrary default}
]
