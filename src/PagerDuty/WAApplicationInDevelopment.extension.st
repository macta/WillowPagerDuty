Extension { #name : #WAApplicationInDevelopment }

{ #category : #'*PagerDuty' }
WAApplicationInDevelopment >> installApplicationFileHandlerFor: aWillowApplication [
	
	WADeploymentAwareFileHandler installAsFileHandlerAccordingTo: self
]
