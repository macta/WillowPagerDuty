"
I am a simple refactoring to provide conveniant functionality shared between web views.
"
Class {
	#name : #WPWebView,
	#superclass : #WAPainter,
	#category : #PagerDuty
}

{ #category : #rendering }
WPWebView >> context [
	^self session applicationContext
]

{ #category : #rendering }
WPWebView >> whileEvaluating: aCalcBlock render: aCanvasBlock on: aCanvas [ 
	
	| delayedView |
	
	delayedView := DelayedViewRenderer
		showing: SpinKitTripleBounce new
		whileEvaluating: aCalcBlock 
		thenRendering: aCanvasBlock.
						
	aCanvas render: delayedView
]
