Extension { #name : #NeoJSONObject }

{ #category : #'*PagerDuty' }
NeoJSONObject >> atPathString: aDelimitedPathString [
	^ self atPath: (aDelimitedPathString substrings: {$/ . $.})
]
